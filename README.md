
# DOCKER SYMFONY

Dockerization of a synfony environment


## Installation

Install Docker Symfony

```bash
    docker-compose up -d
    docker exec -it www_docker_symfony bash
    docker exec www_docker_symfony composer create-project symfony/website-skeleton project
```
    
## Documentation

docker-compose up -d :  allows to initialize docker to launch the different containers
docker exec -it www_docker_symfony bash : allows to run the www_docker_symfony container with an interactive bash
docker exec www_docker_symfony composer create-project symfony/website-skeleton project : allows to run the command to create a synfony skeleton

  
## Badges


[![PHP](https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white)](https://opensource.org/licenses/)
[![AGPL License](https://img.shields.io/badge/Made%20for-VSCode-1f425f.svg)](http://www.gnu.org/licenses/agpl-3.0)
[![AGPL License](  https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)](http://www.gnu.org/licenses/agpl-3.0)

